﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class GaussianBlur : MonoBehaviour
{
    [SerializeField] private Shader shader;

    [Range(2, 100)] [SerializeField] private int iterations = 10;
    [Range(0, 1)] [SerializeField] private float neighborhoodSize = .1f;

    [Range(0, .2f)] [SerializeField] private float standardDeviation = .02f;

    private Material material;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (material == null)
            material = new Material(shader);

        material.SetInt("_Iterations", iterations);
        material.SetFloat("neighborhoodSize", neighborhoodSize);
        material.SetFloat("_StandardDeviation", standardDeviation);

        var tmp = RenderTexture.GetTemporary(source.width, source.height);
        Graphics.Blit(source, destination, material, 0);
        RenderTexture.ReleaseTemporary(tmp);
    }
}
