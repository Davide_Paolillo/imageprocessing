﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class UnsharpMaskingBoxBlur : MonoBehaviour
{
    [SerializeField] private Shader shader;

    [Range(2, 100)] [SerializeField] private int iterations = 10;
    [Range(0, 1)] [SerializeField] private float neighborhoodSize = .1f;
    [Range(0, 1)] [SerializeField] private float maskStrength = .1f;

    private Material material;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (material == null)
            material = new Material(shader);

        material.SetInt("_Iterations", iterations);
        material.SetFloat("neighborhoodSize", neighborhoodSize);
        material.SetTexture("_StartTex", source);
        material.SetFloat("k", maskStrength);

        var tmp = RenderTexture.GetTemporary(source.width, source.height);
        var sTmp = RenderTexture.GetTemporary(source.width, source.height);
        Graphics.Blit(source, tmp, material, 0);
        Graphics.Blit(tmp, sTmp, material, 1);
        Graphics.Blit(sTmp, destination, material, 2);
        RenderTexture.ReleaseTemporary(tmp);
        RenderTexture.ReleaseTemporary(sTmp);
    }
}
