﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class MatrixFilter : MonoBehaviour
{
    [SerializeField] private Shader shader;

    private Material material;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (material == null)
            material = new Material(shader);

        Graphics.Blit(source, destination, material);
    }
}
