﻿Shader "Hidden/NearestNieghbour"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float4 _MainTex_TexelSize;

            float4 SymmetricNearestNeighbor
                (float4 centerColor, sampler2D tex, float2 texCoord, float2 texCoordOffset)
            {
                float4 color0 = tex2D(tex, texCoord + texCoordOffset);
                float4 color1 = tex2D(tex, texCoord - texCoordOffset);
                float3 d0 = color0.rgb - centerColor.rgb;
                float3 d1 = color1.rgb - centerColor.rgb;

                return dot(d0, d0) < dot(d1, d1) ? color0 : color1;
            }

            float4 SymmetricNearestNeighborFilter
                (sampler2D tex, float2 texCoord, float2 texelSize, int halfFilterSize)
            {
                // NOTE:
                // SymmetricNearestNeighborFilter algorithm compare the pixels with point symmetry.
                // So, the result of upper left side and lower right side shows same value.
                // This means the doubled upper left value is same as sum total.

                float  pixels = 1.0f;
                float4 centerColor = tex2D(tex, texCoord);
                float4 outputColor = centerColor;

                for (int y = -halfFilterSize; y < 0; y++)
                {
                    float texCoordOffsetY = y * texelSize.y;

                    for (int x = -halfFilterSize; x <= halfFilterSize; x++)
                    {
                        float2 texCoordOffset = float2(x * texelSize.x, texCoordOffsetY);

                        outputColor += SymmetricNearestNeighbor
                            (centerColor, tex, texCoord, texCoordOffset) * 2.0f;

                        pixels += 2.0f;
                    }
                }

                for (int x = -halfFilterSize; x < 0; x++)
                {
                    float2 texCoordOffset = float2(x * texelSize.x, 0.0f);

                    outputColor += SymmetricNearestNeighbor
                        (centerColor, tex, texCoord, texCoordOffset) * 2.0f;

                    pixels += 2.0f;
                }

                outputColor /= pixels;

                return outputColor;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return SymmetricNearestNeighborFilter(_MainTex, i.uv, _MainTex_TexelSize.xy, 20);
            }
            ENDCG
        }
    }
}
