﻿Shader "Hidden/Laplacian"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #define PI 3.14159265359
            #define E 2.71828182846


            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            static const float LaplacianFilterKernel[9] =
            {
                -1, -1, -1,
                -1,  8, -1,
                -1, -1, -1
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                return o;
            }

            sampler2D _MainTex;
            float4    _MainTex_TexelSize;

            float4 LaplacianFilter(sampler2D tex, float2 texCoord, float2 texelSize)
            {
                float4 color = float4(0, 0, 0, 1);
                int count = 0;

                for (int x = -1; x <= 1; x++)
                {
                    for (int y = -1; y <= 1; y++)
                    {
                        texCoord = float2(texCoord.x + texelSize.x * x,
                                          texCoord.y + texelSize.y * y);
                        color.rgb += tex2D(tex, texCoord).rgb * LaplacianFilterKernel[count];
                        count++;
                    }
                }

                return color;
            }

            float4 frag (v2f i) : SV_Target
            {
                return LaplacianFilter(_MainTex, i.uv, _MainTex_TexelSize.xy);
            }
            ENDCG
        }
    }
}
