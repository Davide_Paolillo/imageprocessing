﻿Shader "Hidden/Dithering"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            static const float4x4 DitherMatrixDot =
            {
                0.74, 0.27, 0.40, 0.60,
                0.80, 0.00, 0.13, 0.94,
                0.47, 0.54, 0.67, 0.34,
                0.20, 1.00, 0.87, 0.07
            };

            static const float4x4 DitherMatrixBayer =
            {
                0.000, 0.500, 0.125, 0.625,
                0.750, 0.250, 0.875, 0.375,
                0.187, 0.687, 0.062, 0.562,
                0.937, 0.437, 0.812, 0.312
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float4 _MainTex_TexelSize;

            float4 DitheringFilterDot(sampler2D tex, float2 texCoord, int2 texSize)
            {
                // NOTE:
                // Use NTSC gray because it doesnt use division.

                float4 color = tex2D(tex, texCoord);
                float  gray  = 0.298912f * color.r + 0.586611f * color.g + 0.114478f * color.b;

                int2 texCoordPx = int2(round((texCoord.x * texSize.x) + 0.5) % 4,
                                       round((texCoord.y * texSize.y) + 0.5) % 4);

                //#ifdef _DITHER_BAYER

                //return DitherMatrixBayer[texCoordPx.x][texCoordPx.y] < gray ? float4(0, 0, 0, color.a) : float4(1, 1, 1, color.a);

                //#else // _DITHER_DOT

                return DitherMatrixDot[texCoordPx.x][texCoordPx.y] < gray ? float4(0, 0, 0, color.a) : float4(1, 1, 1, color.a);

                //#endif
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return DitheringFilterDot(_MainTex, i.uv, _MainTex_TexelSize.zw);
            }
            ENDCG
        }
    }
}
