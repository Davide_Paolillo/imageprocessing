﻿Shader "Hidden/BarrelDistortion"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;

            float2 BarrelDistortion(float2 texCoord, float k1, float k2)
            {
                // NOTE:
                // k1, k2 means strength. 
                // Popular values are k1:0.2 k2:0.01.

                float2 distortedCoord;
                float2 centerOriginCoord = texCoord - 0.5;

                float rr = centerOriginCoord.x * centerOriginCoord.x
                         + centerOriginCoord.y * centerOriginCoord.y;
                float rrrr = rr * rr;
                float distortion = 1 + k1 * rr + k2 * rrrr;

                distortedCoord = centerOriginCoord * distortion;
                distortedCoord += 0.5;

                // CAUTION:
                // Somtimes return under 0 or over 1 value.

                return distortedCoord;
            }


            fixed4 frag (v2f i) : SV_Target
            {
                float2 distortedCoord = BarrelDistortion(i.uv, .2, .01);

                bool outOfRange = distortedCoord.x < 0 || 1 < distortedCoord.x ||
                                  distortedCoord.y < 0 || 1 < distortedCoord.y;

                return outOfRange ? 0 : tex2D(_MainTex, distortedCoord);
            }
            ENDCG
        }
    }
}
